#!/bin/bash

# Docs:
# https://github.com/microsoft/vscode/wiki/How-to-Contribute

TELEMETRY_URLS="(dc\.services\.visualstudio\.com)|(vortex\.data\.microsoft\.com)|(ticino\.blob\.core\.windows\.net)|(www\.microsoft\.com)|(api\.esrp\.microsoft\.com)|(dc\.services\.visualstudio\.com)|(dc\.applicationinsights\.microsoft\.com)|(dc\.applicationinsights\.azure\.com)|(global\.in\.ai\.monitor\.azure\.com)|(global\.in\.ai\.privatelink\.monitor\.azure\.com)|(dc\.trafficmanager\.net)|(weu05-breeziest-in\.cloudapp\.net)|(experimentation\.visualstudio\.com)|(code\.visualstudio\.com)|(current\.cvd\.clamav\.net)"

TELEMETRY_URLS=${TELEMETRY_URLS}"|(img\.shields\.io)|(vsmarketplacebadge\.apphb\.com)|(img\.shields\.io)|(badge\.buildkite\.com)|(raw\.githubusercontent\.com)|(api\.codeclimate\.com)|(codeclimate\.com)"

PURPLE='\033[0;35m'
NC='\033[0m' # No Color

BUILD_TS=$(date '+%s')

echo -e "${PURPLE}===> Removing old builds. Please wait...${NC}"
rm -rf vscode
rm -rf VSCode-linux-x64
rm -rf vscode-reh-web-linux-x64
[ "$1" == "cleanup" ] && exit

echo -e "${PURPLE}===> Cloning VSCode sources. Please wait...${NC}"
git clone https://github.com/microsoft/vscode.git --depth=1

cd vscode || exit 1

echo -e "${PURPLE}===> Updating setting defaults.${NC}"
../update_settings.sh

echo -e "${PURPLE}===> Building. Please wait...${NC}"
# Disable trash-repoter!
sed -i 's/"$CLI"/"$CLI" --disable-crash-reporter/' resources/linux/bin/code.sh

npm install

# Replace telemetry URLs.
grep -rl --exclude-dir=.git -E $TELEMETRY_URLS . | xargs sed -i -E "s/$TELEMETRY_URLS/0\.0\.0\.0/g"

echo -e "${PURPLE}===> Building a generic Linux binary. Please wait...${NC}"
npm run gulp vscode-linux-x64-min

# Electron dictionaries are downloaded on the 
# `FirstRun` procedure, or if 
# `~/.config/Code - OSS/Dictionaries/en-US-9-0.bdic` 
# file is absent. Electron package has the 
# `redirector.gvt1.com` URL hardcoded in its binary. 
# That happens only once.
# In case you still need that dictionary, please
# set the LEAVE_DICTS_IN_PLACE=true
if [ "$LEAVE_DICTS_IN_PLACE" != "true" ]
then
  sed -i 's/redirector.gvt1.com/0000000000000000000/' node_modules/electron/dist/electron
  sed -i 's/redirector.gvt1.com/0000000000000000000/' ../VSCode-linux-x64/code-oss
fi

if [ "$BUILD_APPIMAGE" == "true" ]
then
  echo "
[Desktop Entry]
Name=Uncoded
Type=Application
Exec=AppRun
Icon=AppRun
Categories=TextEditor;Development;IDE;
" > ../VSCode-linux-x64/AppRun.desktop

  # We need an unpacked AppimageTool because Docker doesn't support FUSE.
  wget -c https://github.com/AppImage/appimagetool/releases/download/continuous/appimagetool-x86_64.AppImage
  wget -c https://github.com/AppImage/type2-runtime/releases/download/continuous/runtime-x86_64
  chmod +x appimagetool-x86_64.AppImage
  chmod +x runtime-x86_64
  ./appimagetool-x86_64.AppImage --appimage-extract
  ln -sT code-oss ../VSCode-linux-x64/AppRun
  cp ../VSCode-linux-x64/resources/app/resources/linux/code.png ../VSCode-linux-x64/AppRun.png
  PATH=$PATH:squashfs-root/usr/bin ARCH=x86_64 appimagetool --runtime-file ./runtime-x86_64 ../VSCode-linux-x64
  mv Uncoded-x86_64.AppImage ../code-oss-${BUILD_TS}_amd64.AppImage
fi

if [ "$GENERIC_TARBALL" == "true" ]
then
  echo -e "${PURPLE}===> Building a generic Linux tarball. Please wait...${NC}"
  mkdir -p uncoded/opt/uncoded
  mkdir -p uncoded/usr/share/applications
  mkdir -p uncoded/usr/share/pixmaps
  mkdir -p uncoded/usr/bin
  rsync -a ../VSCode-linux-x64/* uncoded/opt/uncoded
  cp uncoded/opt/uncoded/resources/app/resources/linux/code.png uncoded/usr/share/pixmaps/com.visualstudio.code.oss.png

echo '#!/usr/bin/env sh
/opt/uncoded/bin/code-oss "$@"' > uncoded/usr/bin/code-oss
chmod +x uncoded/usr/bin/code-oss

echo "
[Desktop Entry]
Name=Uncoded
Comment=Code Editing. Redefined.
GenericName=Text Editor
Exec=code-oss --unity-launch %F
Icon=com.visualstudio.code.oss
Type=Application
StartupNotify=false
StartupWMClass=Code - OSS
Categories=TextEditor;Development;IDE;
MimeType=text/plain;inode/directory;application/x-code-oss-workspace;
Actions=new-empty-window;
Keywords=vscode;

[Desktop Action new-empty-window]
Name=New Empty Window
Exec=code-oss --new-window %F
Icon=com.visualstudio.code.oss
" > uncoded/usr/share/applications/code-oss.desktop

  tar czf code-oss-${BUILD_TS}_amd64.tgz -C uncoded .
  mv code-oss-${BUILD_TS}_amd64.tgz ..
fi

if [ "$BUILD_DEB" == "true" ]
then
  echo -e "${PURPLE}===> Building a deb package. Please wait...${NC}"
  alien --target=amd64 --description=Uncoded --to-deb ../code-oss-${BUILD_TS}_amd64.tgz 
  #npm run gulp vscode-linux-x64-build-deb
  mv *.deb ..
fi

#if [ "$BUILD_RPM" == "true" ]
#then
#  echo -e "${PURPLE}===> Building an rpm package. Please wait...${NC}"
#  #alien --target=amd64 --description=Uncoded --to-rpm ../code-oss_${BUILD_TS}-2_amd64.deb
#  npm run gulp vscode-linux-x64-build-rpm
#  mv *.rpm ..
#fi

if [ "$REH_WEB" == "true" ]
then
  echo -e "${PURPLE}===> Building Uncoded-WEB Linux tarball. Please wait...${NC}"
  npm run gulp vscode-reh-web-linux-x64-min
  tar cJf ../code-oss-reh-web-${BUILD_TS}_Linux_amd64.tar.xz ../vscode-reh-web-linux-x64
fi
