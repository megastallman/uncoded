FROM ubuntu:22.04

ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Nicosia
ENV BUILD_DEB=true
ENV BUILD_APPIMAGE=true
ENV GENERIC_TARBALL=true
ENV REH_WEB=true

RUN apt update \
    && apt install wget desktop-file-utils rsync rpm curl git build-essential libkrb5-dev g++ libx11-dev libxkbfile-dev libsecret-1-dev python-is-python3 fakeroot alien -y \
    && apt clean

RUN export NVM_DIR=/opt \
    && curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.1/install.sh | bash \
    && . /opt/nvm.sh \
    && nvm install --lts \
    && nvm use --lts \
    && npm install -g npm@latest

