#!/bin/bash

CONT_ENGINE=${CONT_ENGINE:-docker}

CONTAINER_LOG=/tmp/uncoded-container.log

echo "===> Building an Uncoded builder container. Please wait..."

if [ "$CONT_ENGINE" == "docker" ]
then
    ${CONT_ENGINE} build --network=host . 2> $CONTAINER_LOG
    CONTAINER=$(cat $CONTAINER_LOG | grep "writing image" | awk '{ print $4 }')
	REPLACE_FLAG=''
else
    ${CONT_ENGINE} build --network=host . | tee $CONTAINER_LOG
    CONTAINER=$(tail -n2 $CONTAINER_LOG | grep '\--> ' | cut -f 2 -d' ')
	REPLACE_FLAG=' --replace '
fi

if [ "$CONTAINER" == "" ]
then
	echo "ERROR: Uncoded builder container build failed."
	exit 1
fi

CONTAINER=$(echo $CONTAINER | cut -d' ' -f3)

echo "===> Building UNCODED! Keep tight!"

${CONT_ENGINE} run --rm -it --network=host --name tmp-uncoded-builder ${REPLACE_FLAG} \
	--mount type=bind,source="$(pwd)",target=/uncoded \
	$CONTAINER \
	/bin/bash -c ". /opt/nvm.sh \
	&& cd /uncoded \
	&& ./build.sh \
	&& ./build.sh cleanup \
	&& chmod a+rw code-oss*"
