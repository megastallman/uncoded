DEFAULT_TRUE="'default': true"
DEFAULT_TRUE1="default: true"
DEFAULT_FALSE="'default': false"
DEFAULT_FALSE1="default: false"
DEFAULT_ON="'default': TelemetryConfiguration.ON"
DEFAULT_OFF="'default': TelemetryConfiguration.OFF"

is_gnu_sed () {
  sed --version >/dev/null 2>&1
}

replace () {
  echo "${1}"
  if is_gnu_sed; then
    sed -i -E "${1}" "${2}"
  else
    sed -i '' -E "${1}" "${2}"
  fi
}

update_setting () {
  local FILENAME="${2}"
  # check that the file exists
  if [ ! -f "${FILENAME}" ]; then
    echo "File to update setting in does not exist ${FILENAME}"
    return
  fi

  # go through lines of file, looking for block that contains setting
  local SETTING="${1}"
  local LINE_NUM=0
  while read -r line; do
    local LINE_NUM=$(( $LINE_NUM + 1 ))
    if [[ "${line}" == *"${SETTING}"* ]]; then
      local IN_SETTING=1
    fi
    if [[ ("${line}" == *"${DEFAULT_TRUE}"* || "${line}" == *"${DEFAULT_TRUE1}"* || "${line}" == *"${DEFAULT_ON}"*) && "${IN_SETTING}" == "1" ]]; then
      local FOUND=1
      break
    fi
  done < "${FILENAME}"

  if [[ "${FOUND}" != "1" ]]; then
    echo "${DEFAULT_TRUE} not found for setting ${SETTING} in file ${FILENAME}"
    return
  fi

  # construct line-aware replacement string
  if [[ "${line}" == *"${DEFAULT_TRUE}"* ]]; then
    local DEFAULT_TRUE_TO_FALSE="${LINE_NUM}s/${DEFAULT_TRUE}/${DEFAULT_FALSE}/"
  fi

  if [[ "${line}" == *"${DEFAULT_TRUE1}"* ]]; then
    local DEFAULT_TRUE_TO_FALSE="${LINE_NUM}s/${DEFAULT_TRUE1}/${DEFAULT_FALSE1}/"
  fi

  if [[ "${line}" == *"${DEFAULT_ON}"* ]]; then
    local DEFAULT_TRUE_TO_FALSE="${LINE_NUM}s/${DEFAULT_ON}/${DEFAULT_OFF}/"
  fi

  replace "${DEFAULT_TRUE_TO_FALSE}" "${FILENAME}"
}

update_setting "'telemetry.enableCrashReporter':" src/vs/workbench/electron-sandbox/desktop.contribution.ts
update_setting "'update.showReleaseNotes':" src/vs/platform/update/common/update.config.contribution.ts
update_setting "'update.mode':" src/vs/platform/update/common/update.config.contribution.ts
update_setting "'workbench.settings.enableNaturalLanguageSearch':" src/vs/workbench/contrib/preferences/common/preferencesContribution.ts
update_setting "'extensions.autoUpdate':" src/vs/workbench/contrib/extensions/browser/extensions.contribution.ts
update_setting "'extensions.autoCheckUpdates':" src/vs/workbench/contrib/extensions/browser/extensions.contribution.ts
update_setting " TelemetryConfiguration.ON" src/vs/platform/telemetry/common/telemetryService.ts
